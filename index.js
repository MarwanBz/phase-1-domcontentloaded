// Your code goes here
const p = document.getElementById("text");

document.addEventListener("DOMContentLoaded", function () {
  p.innerHTML = "This is really cool!";
});

console.log(
  "This console.log() fires when index.js loads - before DOMContentLoaded is triggered"
);